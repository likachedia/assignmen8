package com.example.myclass

data class Item( var firstName: String, var lastName: String, var email: String, var imgSrc:Int, var username: UserName? = null) {
    data class UserName(val userName:String)
}

