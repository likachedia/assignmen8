package com.example.myclass

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.myclass.databinding.FirstItemBinding
import com.example.myclass.databinding.SecondItemBinding

typealias ItemClick = (item: Item, position: Int) -> Unit

class ItemAdapter:RecyclerView.Adapter<RecyclerView.ViewHolder>() {


    companion object {
        private const val FIRST_ITEM = 10
        private const val SECOND_ITEM = 11
    }
    private val items = mutableListOf<Item>()
    var callback:ItemClick? = null

    inner class FirstViewHolder(private  val binding: FirstItemBinding):RecyclerView.ViewHolder(binding.root),
    View.OnClickListener{
        private lateinit var model:Item
        fun onBind() {
            model = items[adapterPosition]
            binding.firstName.text = model.firstName
            binding.lastName.text = model.lastName
            binding.email.text = model.email
            binding.profileImg.setImageResource(model.imgSrc)
            binding.update.setOnClickListener(this)
            binding.removed.setOnClickListener {
                items.removeAt(adapterPosition)
                notifyItemRemoved(adapterPosition)
            }
        }

        override fun onClick(p: View?) {
            callback?.invoke(model, adapterPosition)
        }
    }

    inner class SecondViewHolder(private  val binding: SecondItemBinding):RecyclerView.ViewHolder(binding.root),
        View.OnClickListener{
        private lateinit var model:Item
        fun onBind() {
            model = items[adapterPosition]
            binding.firstName.text = model.firstName
            binding.lastName.text = model.lastName
            binding.email.text = model.email
            binding.profileImg.setImageResource(model.imgSrc)
            binding.userName.text = model.username?.userName
            binding.update.setOnClickListener(this)
            binding.removed.setOnClickListener {
                items.removeAt(adapterPosition)
                notifyItemRemoved(adapterPosition)
            }
        }

        override fun onClick(p: View?) {
            callback?.invoke(model, adapterPosition)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
     return   if(viewType == FIRST_ITEM) {
            FirstViewHolder(FirstItemBinding.inflate(LayoutInflater.from(parent.context),parent, false))
        } else {
           SecondViewHolder(SecondItemBinding.inflate(LayoutInflater.from(parent.context),parent, false))
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is FirstViewHolder) {
            holder.onBind()

        } else if (holder is SecondViewHolder) {
            holder.onBind()

        }
    }

    override fun getItemCount() = items.size

    override fun getItemViewType(position: Int) = if (items[position].username != null) SECOND_ITEM else FIRST_ITEM


    fun setData(items:MutableList<Item>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }
}