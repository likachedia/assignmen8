package com.example.myclass

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Parcelable
import android.view.View
import com.example.myclass.databinding.ActivityUpdateBinding

class UpdateActivity : AppCompatActivity() {
    private lateinit var binding: ActivityUpdateBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityUpdateBinding.inflate(layoutInflater)
        setContentView(binding.root)
        this.setDefaultValues()
        binding.imgButton.setOnClickListener { save() }
    }

    private fun save() {
        val user: Parcelable? = User(binding.firstName.text.toString(), binding.lastName.text.toString(),
            binding.email.text.toString(), binding.userName.text.toString())
        intent.putExtra("result", user)

        setResult(RESULT_OK, intent)
        finish()
    }

    private fun setDefaultValues() {
        val user = intent.getParcelableExtra<User>("user")
        binding.firstName.setText(user?.firstName)
        binding.lastName.setText(user?.lastName)
        binding.email.setText(user?.email)

        if(user?.username != null) {
            binding.userName.visibility = View.VISIBLE
            binding.userName.setText(user?.username)
        }
        else {
            binding.userName.visibility = View.GONE
        }
    }
}