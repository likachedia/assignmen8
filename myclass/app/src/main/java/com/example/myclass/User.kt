package com.example.myclass

import android.os.Parcelable
import kotlinx.parcelize.Parcelize

@Parcelize
data class User(val firstName:String, val lastName:String, val email:String, val username:String? = null): Parcelable {

}