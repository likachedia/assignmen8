package com.example.myclass

import android.content.Intent
import android.icu.lang.UCharacter.GraphemeClusterBreak.L
import android.os.Binder
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import android.widget.Toast
import androidx.activity.result.contract.ActivityResultContracts
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.myclass.databinding.ActivityMainBinding
import com.example.myclass.databinding.ActivityUpdateBinding

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var adapter:ItemAdapter
    private lateinit var items: MutableList<Item>
    var editItemIndex: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        items = mutableListOf<Item>()
        init()
        binding.addBtn.setOnClickListener {
            createItem()
        }

    }
private fun init () {
    setData()
    binding.recyclerView.layoutManager = GridLayoutManager(this,2)
    adapter = ItemAdapter()
    binding.recyclerView.adapter = adapter
    adapter.callback = {item, position ->
        editItem(position)
    }

    adapter.setData(items)
}
    private fun setData () {
        items.add(Item("firstname1", "lastname1", "email", R.drawable.image1))
        items.add(Item("firstname2", "lastname1", "email", R.drawable.image2, Item.UserName("username1")))
        items.add(Item("firstname3", "lastname1", "email", R.drawable.image1))
        items.add(Item("firstname4", "lastname1", "email", R.drawable.image2))
        items.add(Item("firstname5", "lastname1", "email", R.drawable.image1))
        items.add(Item("firstname6", "lastname1", "email", R.drawable.image2))
        items.add(Item("firstname7", "lastname1", "email", R.drawable.image1))
        items.add(Item("firstname8", "lastname1", "email", R.drawable.image2))
        items.add(Item("firstname9", "lastname1", "email", R.drawable.image1))
        items.add(Item("firstname10", "lastname1", "email", R.drawable.image2))
        items.add(Item("firstname11", "lastname1", "email", R.drawable.image1))
        items.add(Item("firstname12", "lastname1", "email", R.drawable.image2, Item.UserName("username1")))
        items.add(Item("firstname13", "lastname1", "email", R.drawable.image1))
        items.add(Item("firstname14", "lastname1", "email", R.drawable.image2, Item.UserName("username1")))
    }

    private fun editItem(index: Int) {
        editItemIndex = index;
        val item = items[editItemIndex]
        val intent = Intent(this, UpdateActivity::class.java)

        val user = if(item.username == null) {
            User(item.firstName, item.lastName, item.email)
        } else {
            User(item.firstName, item.lastName, item.email, item.username?.userName)
        }
        intent.putExtra("user", user)
        editItemActivity.launch(intent)
    }

    private val editItemActivity = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == RESULT_OK) {
            val user = it.data?.getParcelableExtra<User>("result")
            items[editItemIndex].firstName = user?.firstName!!
            items[editItemIndex].lastName = user?.lastName
            items[editItemIndex].email = user?.email

            if(items[editItemIndex].username != null) {
                items[editItemIndex].username = Item.UserName(user?.username!!)
            }

            binding.recyclerView.adapter?.notifyDataSetChanged()
        }
    }

    private fun createItem() {
        val intent = Intent(this, UpdateActivity::class.java)
        createItemActivity.launch(intent)
    }

    private val createItemActivity = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if(it.resultCode == RESULT_OK) {
            val user = it.data?.getParcelableExtra<User>("result")
            val newItem = Item(user?.firstName!!, user?.lastName!!, user?.email!!,
                R.drawable.image1)
            items.add(newItem)

            adapter.setData(items)
        }
    }
}